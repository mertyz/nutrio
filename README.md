# Nutrio: healthy products at even lower prices!

SoftUni ReactJS Project

Type: E-commerce

Theme: Bioproducts/Nutrition store

## Installing

### Back-End
In the <ProjectName>/server directory, you can run:
### `npm i`
This will install all the dependencies needed for the back-end.

### Front-End
In the project directory, you can run:
### `npm i` 
This will install all the dependencies needed for the front-end.

### Running the tests

### `npm test`

## Getting Started

In the <ProjectName>/server directory, you can run:
### `node index`

Runs the REST API on port: 9999

In the project directory, you can run:
### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## Built With

* [ReactJS](https://reactjs.org/docs/getting-started.html) - Library for the interface
* [ExpressJS](https://expressjs.com/) - Web app framework used
* [mongoose](https://mongoosejs.com/docs/guide.html) - Object modeling
* [mongoDB](https://www.mongodb.com/) - Database


## Authors

* **Mert Enverov** - *Initial work* - [mertyz](https://bitbucket.org/mertyz/)

## License

This project is licensed under the MIT License

## Acknowledgments

* CodePen.io
* Colorlib.com

