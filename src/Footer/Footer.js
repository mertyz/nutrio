/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="Footer">
                <footer id="colorlib-footer" role="contentinfo">
                    <div className="container">
                        <div className="row row-pb-md">
                            <div className="col footer-col colorlib-widget">
                                <h4>About Nutrio</h4>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
                                <p>
                                </p><ul className="colorlib-social-icons">
                                    <li><a href="javascript:void(0)" onClick={(e) => this.props.redirectToSocial(e)}><i id="twitter" className="fab fa-twitter"></i></ a></li>
                                    <li><a href="javascript:void(0)" id="fb" onClick={(e) => this.props.redirectToSocial(e)}><i id="facebook" className="fab fa-facebook-f"></i></ a></li>
                                    <li><a href="javascript:void(0)" id="linkedin" onClick={(e) => this.props.redirectToSocial(e)}><i id="linkedin" className="fab fa-linkedin"></i></ a></li>
                                    <li><a href="javascript:void(0)" id="dribble" onClick={(e) => this.props.redirectToSocial(e)}><i id="dribble" className="fab fa-dribbble"></i></ a></li>
                                </ul>
                                <p />
                            </div>
                            <div className="col footer-col colorlib-widget">
                                <h4>Customer Care</h4>
                                <p>
                                </p><ul className="list-unstyled">
                                    <li><a href="/contact">Contact</a></li>
                                    <li><a href="/returns">Returns/Exchange</a></li>
                                </ul>
                                <p />
                            </div>
                            <div className="col footer-col colorlib-widget">
                                <h4>Information</h4>
                                <p>
                                </p><ul className="list-unstyled">
                                    <li><a href="/about">About us</a></li>
                                    <li><a href="/delivery-info">Delivery Information</ a></li>
                                    <li><a href="/privacy-policy">Privacy Policy</a></li>
                                </ul>
                                <p />
                            </div>
                            <div className="col footer-col">
                                <h4>News</h4>
                                <ul className="list-unstyled">
                                    <li><a href="/blog">Blog</a></li>
                                </ul>
                            </div>
                            <div className="col footer-col">
                                <h4>Contact Information</h4>
                                <ul className="list-unstyled">
                                    <li>291 South 21th Street, <br /> Suite 721 New York NY 10016</li>
                                    <li><a href="tel://1234567920">+ 1235 2355 98</a></li>
                                    <li><a href="mailto:info@nutrio.com">info@nutrio.com</a></li>
                                    <li><a href="/">nutrio.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="copy">
                        <div className="row">
                            <div className="col-sm-12 text-center">
                                <p>
                                    <span>
                                        Copyright © All rights reserved
                                </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default Footer;