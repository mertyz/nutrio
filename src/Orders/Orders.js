import React, { Component } from 'react';

class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: null
            // title: null,
            // quantity: null,
            // price: null,
            // total: null
        }
    }

    componentWillMount() {
        let username = localStorage.getItem('username');
        fetch("http://localhost:9999/auth/user/orders/" + username)
            .then(rawData => rawData.json())
            .then(body => {
                let orders = body.orders;
                this.setState({
                    orders
                });
            });
    }

    render() {
        if (!this.state.orders) {
            return (
                <div>Loading...</div>
            )
        }
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Your Orders</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="first">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    {this.state.orders && this.state.orders.length > 0 ?
                                        <h1>Your orders</h1>
                                        :
                                        <h1>You still have no orders.</h1>
                                    }
                                </div>
                            </div>
                            <div>
                                {
                                    this.state.orders.map((order, index) => {
                                        return (
                                            <span key={index}>
                                                <h2>Order #{index + 1}</h2>
                                                <table className="table table-dark">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Quanity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {order.products.map((product, i) => {
                                                            return (
                                                                <tr key={i}>
                                                                    <th scope="col">{i + 1}</th>
                                                                    <td>{product.title}</td>
                                                                    <td>${product.price.toFixed(2)}</td>
                                                                    <td>{product.quantity}</td>
                                                                </tr>
                                                            )
                                                        })
                                                        }
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <th scope="col">Delivery</th>
                                                            <td>${order.delivery.toFixed(2)}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <th scope="col">VAT</th>
                                                            <td>${order.VAT.toFixed(2)}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <th scope="col">Total</th>
                                                            <td>${order.total.toFixed(2)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </span>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Orders;