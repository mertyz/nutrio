import React, { Component } from 'react';

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null
        }
        this.handleChange = props.handleChange.bind(this);
    }

    render() {
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Add Category</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="first">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    <h1>Add Category</h1>
                                </div>
                            </div>
                            <form onSubmit={(e) => this.props.handleCreateCategorySubmit(e, this.state)} className="colorlib-form">
                                <div className="form-group">
                                    <label htmlFor="category">Category</label>
                                    <input type="text" onChange={this.handleChange} name="title" className="form-control" id="title" aria-describedby="emailHelp" placeholder="Enter category title" />
                                </div>
                                <div className="col-md-12 text-center ">
                                    <button type="submit" className="btn btn-primary btn-outline-primary">Add Category</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Category;