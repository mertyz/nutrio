import React, { Component } from 'react';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            email: null,
            firstName: null,
            lastName: null,
            password: null
        }
        this.handleChange = props.handleChange.bind(this);
    }

    render() {
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Register</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="second">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    <h1>Sign Up</h1>
                                </div>
                            </div>
                            <form onSubmit={(e) => this.props.handleSubmit(e, this.state, true)} className="colorlib-form">
                                <div className="form-group">
                                    <label htmlFor="username">Username</label>
                                    <input type="text" onChange={this.handleChange} name="username" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter Username" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email address</label>
                                    <input type="email" onChange={this.handleChange} name="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="firstName">First Name</label>
                                    <input type="text" onChange={this.handleChange} name="firstName" className="form-control" id="firstName" aria-describedby="emailHelp" placeholder="Enter Firstname" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input type="text" onChange={this.handleChange} name="lastName" className="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Lastname" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input type="password" onChange={this.handleChange} name="password" id="password" className="form-control" aria-describedby="emailHelp" placeholder="Enter Password (minimum 5 characters)" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="repeatPassword">Confirm Password</label>
                                    <input type="password" onChange={this.handleChange} name="repeatPassword" id="repeatPassword" className="form-control" aria-describedby="emailHelp" placeholder="Repeat Password" required/>
                                </div>
                                <div className="col-md-12 text-center mb-3">
                                    <button type="submit" className="btn btn-block btn-outline-secondary">Sign Up</button>
                                </div>
                                <div className="col-md-12 ">
                                    <div className="form-group">
                                        <p className="text-center">Already have an account? <a href="/login" id="signin">Login here.</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;