import React, { Component } from 'react';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null,
            imageUrl: null,
            price: null,
            nutritionalInfo: null,
            dataSheet: null,
            description: null,
            category: null
        }
        this.handleChange = props.handleChange.bind(this);
    }

    render() {
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Add Product</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="first">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    <h1>Add Product</h1>
                                </div>
                            </div>
                            <form onSubmit={(e) => this.props.handleCreateProductSubmit(e, this.state)} className="colorlib-form">
                                <div className="form-group">
                                    <label htmlFor="selectCategory">Category</label>
                                    <br />
                                    <select className="custom-select" onChange={this.handleChange} id="selectCategory" name="category">
                                        <option value="empty">Select Category</option>
                                        {this.props.categories.map((category, index) => {
                                            return (
                                                <option key={index} value={category._id}>{category.title}</option>
                                            )
                                        })
                                        }
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title">Product Title</label>
                                    <input type="text" onChange={this.handleChange} name="title" id="title" className="form-control" aria-describedby="emailHelp" placeholder="Enter product title" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="imageUrl">Product Image URL</label>
                                    <input type="text" onChange={this.handleChange} name="imageUrl" id="imageUrl" className="form-control" aria-describedby="emailHelp" placeholder="Enter product Image URL" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="price">Product Price</label>
                                    <input type="number" step="0.01" onChange={this.handleChange} name="price" id="price" className="form-control" aria-describedby="emailHelp" placeholder="Enter product price" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="dataSheet">Product Data Sheet</label>
                                    <ul>
                                        <li>
                                            <input id="organic" type="checkbox" onChange={this.handleChange} name="organic" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Organic</label>
                                        </li><br />
                                        <li>
                                            <input id="lactoseFree" type="checkbox" onChange={this.handleChange} name="lactoseFree" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Lactose Free</label>
                                        </li><br />
                                        <li>
                                            <input id="protein" type="checkbox" onChange={this.handleChange} name="protein" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Protein</label>
                                        </li><br />
                                        <li>
                                            <input id="vegan" type="checkbox" onChange={this.handleChange} name="vegan" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Vegan</label>
                                        </li><br />
                                    </ul>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="description">Product Description</label>
                                    <textarea onChange={this.handleChange} name="description" id="description" className="form-control" cols={30} rows={10} placeholder="Enter product description" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="nutritionalInfo">Nutritional Info</label>
                                    <input type="number" step="0.01" onChange={this.handleChange} name="energy" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" placeholder="Enter product energy info (kCal)" />
                                    <input type="number" step="0.01" onChange={this.handleChange} name="protein" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" placeholder="Enter product protein info (g)" />
                                    <input type="number" step="0.01" onChange={this.handleChange} name="carbs" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" placeholder="Enter product carbohydrates info (g)" />
                                    <input type="number" step="0.01" onChange={this.handleChange} name="fats" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" placeholder="Enter product fats info (g)" />
                                </div>
                                <div className="col-md-12 text-center ">
                                    <button type="submit" className="btn btn-block btn-primary btn-outline-primary">Add Product</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Product;