import React, { Component } from 'react';

class EditProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.handleEditChange = props.handleEditChange.bind(this);
    }

    componentWillMount() {
        let productId = this.props.match.params.id;
        fetch('http://localhost:9999/feed/product/' + productId)
            .then(rawData => rawData.json())
            .then(body => {
                this.setState({
                    product: body.product
                })
            });
    }

    render() {
        if (!this.state.product) {
            return <div>Loading...</div>
        }
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Edit Product</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="first">
                        <div className="myform form">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    <h1>Edit Product</h1>
                                </div>
                            </div>
                            <form key={this.state.product._id} onSubmit={(e) => this.props.handleEditProductSubmit(e, this.state)} className="colorlib-form">
                                <div className="form-group">
                                    <label htmlFor="selectCategory">Category</label>
                                    <br />
                                    <select className="custom-select" defaultValue={this.state.product.category} onChange={this.handleEditChange} id="selectCategory" name="category">
                                        <option value="empty">Select Category</option>
                                        {this.props.categories.map((category, index) => {
                                            return (
                                                <option key={index} value={category._id}>{category.title}</option>
                                            )
                                        })
                                        }
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title">Product Title</label>
                                    <input type="text" onChange={this.handleEditChange} name="title" id="title" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.title} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="imageUrl">Product Image URL</label>
                                    <input type="text" onChange={this.handleEditChange} name="imageUrl" id="imageUrl" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.imageUrl} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="price">Product Price</label>
                                    <input type="number" step="0.01" onChange={this.handleEditChange} name="price" id="price" className="form-control" aria-describedby="emailHelp" defaultValue={parseFloat(this.state.product.price).toFixed(2)} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="dataSheet">Product Data Sheet</label>
                                    <ul>
                                        <li>
                                            <input id="organic" defaultChecked={this.state.product.dataSheet.organic} type="checkbox" onChange={this.handleEditChange} name="organic" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Organic</label>
                                        </li><br />
                                        <li>
                                            <input id="lactoseFree" type="checkbox" defaultChecked={this.state.product.dataSheet.lactoseFree} onChange={this.handleEditChange} name="lactoseFree" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Lactose Free</label>
                                        </li><br />
                                        <li>
                                            <input id="protein" type="checkbox" onChange={this.handleEditChange} defaultChecked={this.state.product.dataSheet.protein} name="protein" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Protein</label>
                                        </li><br />
                                        <li>
                                            <input id="vegan" type="checkbox" onChange={this.handleEditChange} defaultChecked={this.state.product.dataSheet.vegan} name="vegan" style={{ float: 'left' }} />
                                            <label style={{ wordWrap: 'break-word', lineHeight: '16px', float: 'left' }}>Vegan</label>
                                        </li><br />
                                    </ul>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="description">Product Description</label>
                                    <textarea onChange={this.handleEditChange} name="description" id="description" className="form-control"  cols={30} rows={10} defaultValue={this.state.product.description} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="nutritionalInfo">Nutritional Info</label>
                                    <input type="number" step="0.01" onChange={this.handleEditChange} name="energy" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.nutritionalInfo.energy} />
                                    <input type="number" step="0.01" onChange={this.handleEditChange} name="protein" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.nutritionalInfo.protein} />
                                    <input type="number" step="0.01" onChange={this.handleEditChange} name="carbs" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.nutritionalInfo.carbs} />
                                    <input type="number" step="0.01" onChange={this.handleEditChange} name="fats" id="nutritionalInfo" className="form-control" aria-describedby="emailHelp" defaultValue={this.state.product.nutritionalInfo.fats} />
                                </div>
                                <div className="col-md-12 text-center ">
                                    <button type="submit" className="btn btn-block btn-primary btn-outline-primary">Edit Product</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default EditProduct;