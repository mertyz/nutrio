import React, { Component } from 'react';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentProduct: null
        }
    }

    componentWillMount() {
        const productId = this.props.match.params.id;
        fetch('http://localhost:9999/feed/product/' + productId)
            .then(rawData => rawData.json())
            .then(body => {
                this.setState({
                    currentProduct: body.product
                })
            });
    }

    render() {
        if (!this.state.currentProduct) {
            return <div>Loading...</div>
        }
        return (
            <div className="ProductDetails">
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="/">Home</a></span> / <span>Product Details</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-product">
                    <div className="container">
                        <div className="row row-pb-lg product-detail-wrap">
                            <div className="col-sm-6">
                                <div className="item">
                                    <div className="product-entry border">
                                        <img src={this.state.currentProduct.imageUrl} className="img-fluid" alt="Free html5 bootstrap 4 template" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="product-desc">
                                    <h3>{this.state.currentProduct.title}</h3>
                                    <p className="price">
                                        <span>${this.state.currentProduct.price.toFixed(2)}</span>
                                    </p>
                                    {
                                        this.state.currentProduct.description.length > 500 ?
                                            <p align="justify">{this.state.currentProduct.description.substring(0, 500) + '...'}</p>
                                            :
                                            <p align="justify">{this.state.currentProduct.description}</p>
                                    }
                                    <div className="row">
                                        <div className="col-sm-12 text-center">
                                            <p className="addtocart"><button onClick={() => this.props.addToCart(this.state.currentProduct)} className="btn btn-primary btn-addtocart"><i className="icon-shopping-cart" /> Add to Cart</button></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="row">
                                    <div className="col-md-12 pills">
                                        <div className="bd-example bd-example-tabs">
                                            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                <li className="nav-item">
                                                    <a className="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-expanded="true">Description</a>
                                                </li>
                                                {this.state.currentProduct.dataSheet ? (
                                                    <li className="nav-item">
                                                        <a className="nav-link" id="pills-manufacturer-tab" data-toggle="pill" href="#pills-manufacturer" role="tab" aria-controls="pills-manufacturer" aria-expanded="true">Data Sheet</a>
                                                    </li>
                                                ) : null}
                                                {this.state.currentProduct.nutritionalInfo ?
                                                    (
                                                        <li className="nav-item">
                                                            <a className="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-expanded="true">Nutritional Info</a>
                                                        </li>
                                                    ) : null
                                                }
                                            </ul>
                                            <div className="tab-content" id="pills-tabContent">
                                                <div className="tab-pane border fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                                                    <h2>WHY YOU'LL LOVE IT</h2>
                                                    <p align="justify">{this.state.currentProduct.description}</p>
                                                </div>
                                                <div className="tab-pane border fade" id="pills-manufacturer" role="tabpanel" aria-labelledby="pills-manufacturer-tab">
                                                    {this.state.currentProduct.dataSheet ? (
                                                        <table className="table">
                                                            <tbody>
                                                                {
                                                                    this.state.currentProduct.dataSheet.organic ?
                                                                        (
                                                                            <tr className="odd">
                                                                                <td><img className="datasheet-icon" src="/images/43.png" alt="Organic" /> Organic</td></tr>
                                                                        )
                                                                        :
                                                                        null
                                                                }
                                                                {
                                                                    this.state.currentProduct.dataSheet.lactoseFree ?
                                                                        (
                                                                            <tr className="even">
                                                                                <td><img className="datasheet-icon" src="/images/31.png" alt="Lactose Free" /> Lactose free</td></tr>
                                                                        )
                                                                        :
                                                                        null
                                                                }
                                                                {
                                                                    this.state.currentProduct.dataSheet.protein ?
                                                                        (
                                                                            <tr className="odd">
                                                                                <td><img className="datasheet-icon" src="/images/28.png" alt="Protein" /> Protein</td></tr>
                                                                        )
                                                                        :
                                                                        null
                                                                }
                                                                {
                                                                    this.state.currentProduct.dataSheet.vegan ?
                                                                        (
                                                                            <tr className="even">
                                                                                <td><img className="datasheet-icon" src="/images/26.png" alt="Vegan" /> Vegan</td></tr>
                                                                        )
                                                                        :
                                                                        null
                                                                }
                                                            </tbody>
                                                        </table>
                                                    ) : null
                                                    }
                                                </div>
                                                <div className="tab-pane border fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                                    {this.state.currentProduct.nutritionalInfo ?
                                                        (
                                                            <table className="table-fill">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                        </th>
                                                                        <th>Value for 100g (g)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {
                                                                        this.state.currentProduct.nutritionalInfo.energy ?
                                                                            (
                                                                                <tr>
                                                                                    <td>Energy (kcal)</td>
                                                                                    <td>{this.state.currentProduct.nutritionalInfo.energy}</td>
                                                                                </tr>
                                                                            )
                                                                            :
                                                                            <tr>
                                                                                <td>Energy (kcal)</td>
                                                                                <td>0</td>
                                                                            </tr>
                                                                    }
                                                                    {
                                                                        this.state.currentProduct.nutritionalInfo.protein ?
                                                                            (
                                                                                <tr>
                                                                                    <td>Protein</td>
                                                                                    <td>{this.state.currentProduct.nutritionalInfo.protein}g</td>
                                                                                </tr>
                                                                            )
                                                                            :
                                                                            <tr>
                                                                                <td>Protein</td>
                                                                                <td>0.0g</td>
                                                                            </tr>
                                                                    }
                                                                    {
                                                                        this.state.currentProduct.nutritionalInfo.carbs ?
                                                                            (
                                                                                <tr>
                                                                                    <td>Carbohydrates</td>
                                                                                    <td>{this.state.currentProduct.nutritionalInfo.carbs}g</td>
                                                                                </tr>
                                                                            )
                                                                            :
                                                                            <tr>
                                                                                <td>Carbohydrates</td>
                                                                                <td>0.0g</td>
                                                                            </tr>
                                                                    }
                                                                    {
                                                                        this.state.currentProduct.nutritionalInfo.fats ?
                                                                            (
                                                                                <tr>
                                                                                    <td>Fats</td>
                                                                                    <td>{this.state.currentProduct.nutritionalInfo.fats}g</td>
                                                                                </tr>
                                                                            )
                                                                            :
                                                                            <tr>
                                                                                <td>Fats</td>
                                                                                <td>0.0g</td>
                                                                            </tr>
                                                                    }
                                                                </tbody>
                                                            </table>
                                                        ) : null
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Details;