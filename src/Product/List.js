/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCategory: {},
            currentProducts: null
        }
    }

    componentWillMount() {
        const link = this.props.match.params.link;
        console.log(this.props.location.query);
        if (link) {
            fetch('http://localhost:9999/feed/category/filter/' + link)
                .then(rawData => rawData.json())
                .then(body => {
                    this.setState({
                        currentCategory: body.category,
                        currentProducts: body.category.products
                    })
                });
        }
    }

    orderCurrentProductsByDate() {
        let products = [...this.state.currentProducts];
        if (products) {
            let sortedProducts = products.sort(function (a, b) {
                var key1 = new Date(a.dateCreated);
                var key2 = new Date(b.dateCreated);

                if (key1 > key2) {
                    return -1;
                } else if (key1 === key2) {
                    return 0;
                } else {
                    return 1;
                }
            });
            this.setState({
                currentProducts: sortedProducts
            });
        }
    }

    orderCurrentProductsByTitle() {
        let products = [...this.state.currentProducts];
        if (products) {
            let sortedProducts = products.sort((a, b) => {
                let key1 = a.title.toLowerCase();
                let key2 = b.title.toLowerCase();

                return (key1 < key2) ? -1 : (key1 > key2) ? 1 : 0;
            });
            this.setState({
                currentProducts: sortedProducts
            });
        }
    }

    orderCurrentProductsDesc() {
        let products = [...this.state.currentProducts];
        if (products) {
            let sortedProducts = products.sort((a, b) => {
                let key1 = a.title.toLowerCase();
                let key2 = b.title.toLowerCase();

                return (key1 > key2) ? -1 : (key1 < key2) ? 1 : 0;
            });
            this.setState({
                currentProducts: sortedProducts
            });
        }
    }

    orderCurrentProductsByPriceAsc() {
        let products = [...this.state.currentProducts];
        if (products) {
            let sortedProducts = products.sort((a, b) => {
                let key1 = a.price;
                let key2 = b.price;

                return (key1 < key2) ? -1 : (key1 > key2) ? 1 : 0;
            });
            this.setState({
                currentProducts: sortedProducts
            });
        }
    }

    orderCurrentProductsByPriceDesc() {
        let products = [...this.state.currentProducts];
        if (products) {
            let sortedProducts = products.sort((a, b) => {
                let key1 = a.price;
                let key2 = b.price;

                return (key1 > key2) ? -1 : (key1 < key2) ? 1 : 0;
            });
            this.setState({
                currentProducts: sortedProducts
            });
        }
    }

    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                {
                                    this.state.currentProducts ?
                                        <p className="bread"><span><a href="/">Home</a></span> / <span>{this.state.currentCategory.title}</span></p>
                                        :
                                        <p className="bread"><span><a href="/">Home</a></span> / <span>Products</span></p>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="breadcrumbs-two">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                {
                                    this.state.currentProducts ?
                                        <div>
                                            <h1>{this.state.currentCategory.title}</h1>
                                        </div>
                                        :
                                        <div>
                                            <h1>Products</h1>
                                        </div>
                                }
                                <div className="menu text-center">
                                    {
                                        this.state.currentProducts ?
                                            <p><a href="javascript:void(0)" onClick={() => this.orderCurrentProductsByDate()}>New Arrivals</a> <a href="javascript:void(0)" onClick={() => this.orderCurrentProductsByTitle()}>A-Z</a> <a href="javascript:void(0)" onClick={() => this.orderCurrentProductsDesc()}>Z-A</a> <a href="javascript:void(0)" onClick={() => this.orderCurrentProductsByPriceAsc()}>Price (A-Z)</a><a href="javascript:void(0)" onClick={() => this.orderCurrentProductsByPriceDesc()}>Price (Z-A)</a></p>
                                            :
                                            <p><a href="javascript:void(0)" onClick={() => this.props.orderProductsByDate()}>New Arrivals</a> <a href="javascript:void(0)" onClick={() => this.props.orderProductsByTitle()}>A-Z</a> <a href="javascript:void(0)" onClick={() => this.props.orderProductsDesc()}>Z-A</a> <a href="javascript:void(0)" onClick={() => this.props.orderProductsByPriceAsc()}>Price (A-Z)</a><a href="javascript:void(0)" onClick={() => this.props.orderProductsByPriceDesc()}>Price (Z-A)</a></p>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-product">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>View All Products</h2>
                            </div>
                        </div>
                        <div className="row row-pb-md">
                            {
                                this.state.currentProducts ?
                                    this.state.currentProducts.map((product, index) => {
                                        return (
                                            <div key={index} className="col-md-3 col-lg-3 mb-4 text-center">
                                                <div className="product-entry border">
                                                    <a href={'/details/' + product._id} className="prod-img">
                                                        <img src={product.imageUrl} className="img-fluid" alt="Free html5 bootstrap 4 template" />
                                                    </a>
                                                    <div className="desc">
                                                        <h2><a href={'/details/' + product._id}>{product.title}</a></h2>
                                                        <span className="price">${product.price.toFixed(2)}</span>
                                                    </div>
                                                    {
                                                        this.props.isAdmin ?
                                                            <div>
                                                                <br />
                                                                <span className='alignBottom'>
                                                                    <a href={'/remove/' + product._id} className="btn btn-danger btn-sm"><i className="fas fa-times"></i></a>
                                                                    <a href={'/edit/' + product._id} className="btn btn-info btn-sm"><i className="fas fa-pen"></i></a>
                                                                </span>
                                                            </div>
                                                            :
                                                            null
                                                    }
                                                </div>
                                            </div>
                                        )
                                    }) : null
                            }
                            {
                                this.props.products ?
                                    this.props.products.map((product, index) => {
                                        return (
                                            <div key={index} className="col-md-3 col-lg-3 mb-4 text-center">
                                                <div className="product-entry border">
                                                    <a href={'/details/' + product._id} className="prod-img">
                                                        <img src={product.imageUrl} className="img-fluid" alt="Free html5 bootstrap 4 template" />
                                                    </a>
                                                    <div className="desc">
                                                        <h2><a href={'/details/' + product._id}>{product.title}</a></h2>
                                                        <span className="price">${product.price.toFixed(2)}</span>
                                                    </div>
                                                    {
                                                        this.props.isAdmin ?
                                                            <div>
                                                                <br />
                                                                <span className='alignBottom'>
                                                                    <a href={'/remove/' + product._id} className="btn btn-danger btn-sm"><i className="fas fa-times"></i></a>
                                                                    <a href={'/edit/' + product._id} className="btn btn-info btn-sm"><i className="fas fa-pen"></i></a>
                                                                </span>
                                                            </div>
                                                            :
                                                            null
                                                    }
                                                </div>
                                            </div>
                                        )
                                    })
                                    :
                                    null
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default List;