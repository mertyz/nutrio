/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';
import Slider from './Slider/Slider';

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <Slider />
                <div className="colorlib-intro">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12 text-center">
                                <h2 className="intro">Let food be your medicine and medicine be your food</h2>
                                <h2 className="alignright">Hippocrates</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-product">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading">
                                <h2>Best Sellers</h2>
                            </div>
                        </div>
                        <div className="row row-pb-md">
                            {this.props.products.map((product, index) => {
                                return (
                                    <div key={index} className="col-lg-3 mb-4 text-center">
                                        <div className="product-entry border">
                                            <a href={'/details/' + product._id} onMouseEnter={() => { this.props.productHoverEvent(product._id) }} className="prod-img">
                                                <img src={product.imageUrl} className="img-fluid" alt="Free html5 bootstrap 4 template" />
                                            </a>
                                            <div className="desc">
                                                <h2><a href={'/details/' + product._id} onMouseEnter={() => this.props.productHoverEvent(product._id)}>{product.title}</a>
                                                </h2>
                                                <span className="price">${product.price.toFixed(2)}</span>
                                            </div>
                                            {
                                                this.props.isAdmin?
                                                    <div>
                                                        <br />
                                                        <span className='alignBottom'>
                                                            <a href={'/remove/' + product._id} className="btn btn-danger btn-sm"><i className="fas fa-times"></i></a>
                                                            <a href={'/edit/' + product._id} className="btn btn-info btn-sm"><i className="fas fa-pen"></i></a>
                                                        </span>
                                                    </div>
                                                    :
                                                    null
                                            }
                                        </div>
                                    </div>
                                )
                            })
                            }
                        </div>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;