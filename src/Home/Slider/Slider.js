import React, { Component } from 'react';

class Slider extends Component {
    render() {
        return (
            <div className="Slider">
                <aside id="colorlib-hero" className="slide">
                    <div className="flexslider">
                        <ul className="slides">
                            <li style={{ backgroundImage: 'url(images/Superfoods.png)' }}>
                                <div className="overlay" />
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-6 offset-sm-3 text-center slider-text">
                                            <div className="slider-text-inner">
                                                <div className="desc">
                                                    <h1 className="head-1">Women's</h1>
                                                    <h2 className="head-2">Healthy</h2>
                                                    <h2 className="head-3">Products</h2>
                                                    <p className="category"><span>Beauty and hygiene</span></p>
                                                    <p><a href="/product/all" className="btn btn-primary">Shop Nutrition</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style={{ backgroundImage: 'url(images/slide-2.png)' }}>
                                <div className="overlay" />
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-6 offset-sm-3 text-center slider-text">
                                            <div className="slider-text-inner">
                                                <div className="desc">
                                                    <h1 className="head-1">Huge</h1>
                                                    <h2 className="head-2">Sale</h2>
                                                    <h2 className="head-3"><strong className="font-weight-bold">50%</strong> Off</h2>
                                                    <p className="category"><span>Big sale men hygiene</span></p>
                                                    <p><a href="/product/all" className="btn btn-primary">Shop Nutrition</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style={{ backgroundImage: 'url(images/Slide1-3.jpg)' }}>
                                <div className="overlay" />
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-6 offset-sm-3 text-center slider-text">
                                            <div className="slider-text-inner">
                                                <div className="desc">
                                                    <h1 className="head-1">New</h1>
                                                    <h2 className="head-2">Arrival</h2>
                                                    <h2 className="head-3">up to <strong className="font-weight-bold">30%</strong> off</h2>
                                                    <p className="category"><span>New healthy food</span></p>
                                                    <p><a href="/product/all" className="btn btn-primary">Shop Nutrition</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        );
    }
};

export default Slider;