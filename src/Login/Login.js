import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        }
        this.handleChange = props.handleChange.bind(this);
    }

    render() {
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Login</span></p>
                        </div>
                    </div>
                </div>
                <div className="col-md-5 mx-auto">
                    <div id="first">
                        <div className="myform form ">
                            <div className="logo mb-3">
                                <div className="col-md-12 text-center">
                                    {
                                        this.props.message ?
                                            <h2 style={{ color: "red" }}>{this.props.message}</h2>
                                            :
                                            null
                                    }
                                    <h1>Login</h1>
                                </div>
                            </div>
                            <form onSubmit={(e) => this.props.handleSubmit(e, this.state, false)} className="colorlib-form">
                                <div className="form-group">
                                    <label htmlFor="username">Username</label>
                                    <input type="text" onChange={this.handleChange} name="username" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter Username" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input type="password" onChange={this.handleChange} name="password" id="password" className="form-control" aria-describedby="emailHelp" placeholder="Enter Password" required/>
                                </div>
                                <div className="form-group">
                                    <p className="text-center">By signing up you accept our <a href="/terms-of-use">Terms Of Use</a></p>
                                </div>
                                <div className="col-md-12 text-center ">
                                    <button type="submit" className="btn btn-block btn-outline-secondary">Login</button>
                                </div>
                                <div className="form-group">
                                    <p className="text-center">Don't have account? <a href="/register" id="signup">Sign up here.</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;