import React, { Component } from 'react';

class Cart extends Component {
    render() {
        return (
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="bread"><span><a href="/">Home</a></span> / <span>Shopping Cart</span></p>
                        </div>
                    </div>
                </div>
                <div className="colorlib-product">
                    <div className="container">
                        <div className="row row-pb-lg">
                            <div className="col-md-10 offset-md-1">
                                <div className="process-wrap">
                                    <div className="process text-center active">
                                        <p><span>01</span></p>
                                        <h3>Shopping Cart</h3>
                                    </div>
                                    <div className="process text-center">
                                        <p><span>02</span></p>
                                        <h3>Checkout</h3>
                                    </div>
                                    <div className="process text-center">
                                        <p><span>03</span></p>
                                        <h3>Order Complete</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row row-pb-lg">
                            <div className="col-md-12">
                                <div className="product-name d-flex">
                                    <div className="one-forth text-left px-4">
                                        <span>Product Details</span>
                                    </div>
                                    <div className="one-eight text-center">
                                        <span>Price</span>
                                    </div>
                                    <div className="one-eight text-center">
                                        <span>Quantity</span>
                                    </div>
                                    <div className="one-eight text-center">
                                        <span>Total</span>
                                    </div>
                                    <div className="one-eight text-center px-4">
                                        <span>Remove</span>
                                    </div>
                                </div>
                                {
                                    this.props.cart && this.props.cart.length > 0 ?
                                        this.props.cart.map((product, index) => {
                                            return (
                                                <div key={product._id} className="product-cart d-flex">
                                                    <div className="one-forth">
                                                        <div className="product-img" style={{ backgroundImage: `url(${product.imageUrl})` }}>
                                                        </div>
                                                        <div className="display-tc">
                                                            <h3>{product.title}</h3>
                                                        </div>
                                                    </div>
                                                    <div className="one-eight text-center">
                                                        <div className="display-tc">
                                                            <span className="price">${product.price}</span>
                                                        </div>
                                                    </div>
                                                    <div className="one-eight text-center">
                                                        <div className="display-tc">
                                                            <input type="text" id={index} name="quantity" className="form-control input-number text-center" defaultValue={product.quantity} min={1} max={100} onChange={this.props.handleQuantityChange} />
                                                        </div>
                                                    </div>
                                                    <div className="one-eight text-center">
                                                        <div className="display-tc">
                                                            <span className="price">${(product.price * product.quantity).toFixed(2)}</span>
                                                        </div>
                                                    </div>
                                                    <div className="one-eight text-center">
                                                        <div className="display-tc">
                                                            <button className="btn btn-outline-danger" onClick={() => this.props.removeCartProduct(index)}>X</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                        <h1 align="center">Your cart is empty</h1>
                                }
                            </div>
                        </div>
                        <div className="row row-pb-lg">
                            <div className="col-md-12">
                                <div className="total-wrap">
                                    <div className="row">
                                        <div className="col-sm-8">
                                        </div>
                                        <div className="col-sm-4 text-center">
                                            {
                                                this.props.cart && this.props.cart.length > 1 ?
                                                    <span>
                                                        <div className="total">
                                                            <div className="sub">
                                                                <p>
                                                                    <span>Subtotal:</span>
                                                                    <span>
                                                                        ${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2)}
                                                                    </span>
                                                                </p>
                                                                <p><span>Delivery:</span> <span>${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2) > 100 ? '0.00' : '5.00'} </span></p>
                                                                <p><span>VAT(20%):</span> <span>${(this.props.cart.reduce((a, b) =>
                                                                    (a + (b.price * b.quantity)), 0) * 0.2).toFixed(2)}</span></p>
                                                            </div>
                                                            <div className="grand-total">
                                                                <p><span><strong>Total:</strong></span> <span>
                                                                    ${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2) > 100 ?
                                                                        ((this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0) * 0.2) + (this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0))).toFixed(2) :
                                                                        ((this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0) * 0.2) + 5 + (this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0))).toFixed(2)
                                                                    }</span></p>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div className="row row-pb-lg">
                                                            <div className="col">
                                                                <a href="/product/all" className="btn btn-primary btn-outline-primary"><i className="icon-shopping-cart"></i> Continue Shopping</a>
                                                            </div>
                                                            <div className="col">
                                                                <a href="/checkout" className="btn btn-primary btn-outline-primary" onClick={() => this.props.handleCart(this.props.cart)}>Checkout</a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    :
                                                    null
                                            }
                                            {
                                                this.props.cart.length === 1 ?
                                                    <span>
                                                        <div className="total">
                                                            <div className="sub">
                                                                <p><span>Subtotal:</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2)}</span></p>
                                                                <p><span>Delivery:</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2) > 100 ? '0.00' : '5.00'}</span></p>
                                                                <p><span>VAT(20%):</span> <span>${((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2).toFixed(2)}</span></p>
                                                            </div>
                                                            <div className="grand-total">
                                                                <p><span><strong>Total:</strong></span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2) > 100 ? ((this.props.cart[0].price * this.props.cart[0].quantity) + 0 + ((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2)).toFixed(2) : ((this.props.cart[0].price * this.props.cart[0].quantity) + 5 + ((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2)).toFixed(2)}</span></p>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div className="row row-pb-lg">
                                                            <div className="col">
                                                                <a href="/product/all" className="btn btn-primary btn-outline-primary"><i className="icon-shopping-cart"></i> Continue Shopping</a>
                                                            </div>
                                                            <div className="col">
                                                                <a href="/checkout" className="btn btn-primary btn-outline-primary" onClick={() => this.props.handleCart(this.props.cart)}>Checkout</a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    :
                                                    null
                                            }
                                            {
                                                !this.props.cart || this.props.cart.length === 0 ?
                                                    <span>
                                                        <div className="total">
                                                            <div className="sub">
                                                                <p><span>Subtotal:</span> <span>$0.00</span></p>
                                                                <p><span>Delivery:</span> <span>$0.00</span></p>
                                                                <p><span>VAT(20%):</span> <span>$0.00</span></p>
                                                            </div>
                                                            <div className="grand-total">
                                                                <p><span><strong>Total:</strong></span> <span>$0.00</span></p>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div className="row row-pb-lg">
                                                            <div className="col">
                                                                <a href="/product/all" className="btn btn-primary"><i className="icon-shopping-cart"></i> Continue Shopping</a>
                                                            </div>
                                                            <div className="col">
                                                                <a href="/checkout" style={{pointerEvents: 'none'}} className="btn btn-primary btn-outline-disabled">Checkout</a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    :
                                                    null
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Cart;