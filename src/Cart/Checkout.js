import React, { Component } from 'react';

class Checkout extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>Checkout</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-product">
                    <div className="container">
                        <div className="row row-pb-lg">
                            <div className="col-sm-10 offset-md-1">
                                <div className="process-wrap">
                                    <a href="/cart">
                                        <div className="process text-center active">
                                            <p><span>01</span></p>
                                            <h3>Shopping Cart</h3>
                                        </div>
                                    </a>
                                    <div className="process text-center active">
                                        <p><span>02</span></p>
                                        <h3>Checkout</h3>
                                    </div>
                                    <div className="process text-center">
                                        <p><span>03</span></p>
                                        <h3>Order Complete</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-8">
                                <form className="colorlib-form" id="checkoutForm" onSubmit={(e) => this.props.clearCart(e)}>
                                    <h2>Billing Details</h2>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label htmlFor="country">Select Country</label>
                                                <div className="form-field">
                                                    <i className="icon icon-arrow-down3" />
                                                    <select name="people" id="people" className="form-control">
                                                        <option value="#">Select country</option>
                                                        <option value="alaska">Alaska</option>
                                                        <option value="bulgaria">Bulgaria</option>
                                                        <option value="china">China</option>
                                                        <option value="germany">Germany</option>
                                                        <option value="italy">Italy</option>
                                                        <option value="japan">Japan</option>
                                                        <option value="korea">Korea</option>
                                                        <option value="philipines">Philippines</option>
                                                        <option value="france">France</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="fname">First Name</label>
                                                <input type="text" id="firstName" className="form-control" placeholder="Your firstname" required/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="lname">Last Name</label>
                                                <input type="text" id="lastName" className="form-control" placeholder="Your lastname" required/>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label htmlFor="companyname">Company Name</label>
                                                <input type="text" id="companyName" className="form-control" placeholder="Company Name" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label htmlFor="fname">Address</label>
                                                <input type="text" id="address" className="form-control" placeholder="Enter Your Address" required/>
                                            </div>
                                            <div className="form-group">
                                                <input type="text" id="address2" className="form-control" placeholder="Second Address" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label htmlFor="companyname">Town/City</label>
                                                <input type="text" id="townCity" className="form-control" placeholder="Town or City" required/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="stateprovince">State/Province</label>
                                                <input type="text" id="fname" className="form-control" placeholder="State Province" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="lname">Zip/Postal Code</label>
                                                <input type="text" id="zippostalcode" className="form-control" placeholder="Zip / Postal" required/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="email">E-mail Address</label>
                                                <input type="text" id="email" className="form-control" placeholder="State Province" required/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="Phone">Phone Number</label>
                                                <input type="text" id="phone" className="form-control" placeholder="Phone Number" required/>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-lg-4">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="cart-detail">
                                            <h2>Cart Total</h2>
                                            {this.props.cart.length > 1 ?
                                                <ul>
                                                    <li>
                                                        <span>Subtotal</span> <span>${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2)}</span>
                                                        <ul>
                                                            {this.props.cart.map((p, index) => {
                                                                return (
                                                                    <li key={p._id}><span>{p.quantity} x {p.title}</span> <span>${(p.quantity * p.price).toFixed(2)}</span></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </li>
                                                    <li><span>Shipping</span> <span>${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2) > 100 ? '0.00' : '5.00'}</span></li>
                                                    <li><span>VAT(20%)</span> <span>${(this.props.cart.reduce((a, b) =>
                                                        (a + (b.price * b.quantity)), 0) * 0.2).toFixed(2)}</span></li>
                                                    <li><span>Order Total</span> <span>${(this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0)).toFixed(2) > 100 ?
                                                        ((this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0) * 0.2) + (this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0))).toFixed(2) :
                                                        ((this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0) * 0.2) + 5 + (this.props.cart.reduce((a, b) => a + (b.price * b.quantity), 0))).toFixed(2)
                                                    }</span></li>
                                                </ul>
                                                :
                                                <ul>
                                                    <li>
                                                        <span>Subtotal</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2)}</span>
                                                        <ul>
                                                            <li key={this.props.cart[0]._id}><span>{this.props.cart[0].quantity} x {this.props.cart[0].title}</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2)}</span></li>
                                                        </ul>
                                                    </li>
                                                    <li><span>Shipping</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2) > 100 ? '0.00' : '5.00'}</span></li>
                                                    <li><span>VAT(20%)</span> <span>${((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2).toFixed(2)}</span></li>
                                                    <li><span>Order Total</span> <span>${(this.props.cart[0].price * this.props.cart[0].quantity).toFixed(2) > 100 ? ((this.props.cart[0].price * this.props.cart[0].quantity) + 0 + ((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2)).toFixed(2) : ((this.props.cart[0].price * this.props.cart[0].quantity) + 5 + ((this.props.cart[0].price * this.props.cart[0].quantity) * 0.2)).toFixed(2)
                                                    }
                                                    </span></li>
                                                </ul>
                                            }
                                        </div>
                                    </div>
                                    <div className="w-100" />
                                    <div className="col-md-12">
                                        <div className="cart-detail">
                                            <h2>Payment Method</h2>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <div className="radio">
                                                        <label><input type="radio" name="payment" defaultChecked /> Direct Bank Tranfer</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <div className="radio">
                                                        <label><input type="radio" name="payment" /> Check Payment</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <div className="radio">
                                                        <label><input type="radio" name="payment" /> Paypal</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="col">
                                            <button form="checkoutForm" className="btn btn-primary btn-outline-primary">Place an order</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Checkout;