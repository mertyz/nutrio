import React, { Component } from 'react';
import { Route, Switch, Redirect, Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Home from '../Home/Home';
import Login from '../Login/Login';
import Register from '../Register/Register';
import CreateProduct from '../Product/Create';
import Category from '../Category/Category';
import Details from '../Product/Details';
import ProductList from '../Product/List';
import Cart from '../Cart/Cart';
import Checkout from '../Cart/Checkout';
import Order from '../Cart/Order';
import EditProduct from '../Product/Edit';
import DeleteProduct from '../Product/Delete';
import Orders from '../Orders/Orders';
import About from '../Common/About';
import Contact from '../Common/Contact';
import { isNull } from 'util';
import Returns from '../Common/Returns';
import Delivery from '../Common/Delivery';
import PrivacyPolicy from '../Common/PrivacyPolicy';
import Terms from '../Common/Terms';
import Blog from '../Common/Blog';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      isAdmin: false,
      products: [],
      categories: [],
      cart: [],
      selectedProductId: null,
      search: null,
    };
    this.onLogout = this.onLogout.bind(this);
    this.addToCart = this.addToCart.bind(this);
  }

  componentWillMount() {
    const isAdmin = localStorage.getItem('isAdmin') === 'true' ? true : false;
    const cart = localStorage.getItem('cart');
    if (localStorage.getItem('username')) {
      this.setState({
        username: localStorage.getItem('username'),
        isAdmin
      });
    }

    if (cart) {
      this.setState({
        cart: JSON.parse(cart)
      })
    }

    Promise.all([
      fetch('http://localhost:9999/feed/categories')
        .then(rawData => rawData.json())
        .then(body => {
          this.setState({
            categories: body.categories
          });
        }),
      fetch('http://localhost:9999/feed/products')
        .then(rawData => rawData.json())
        .then(body => {
          this.setState({
            products: body.products
          });
        })
    ]
    );
  }

  handleSubmit(e, data, isSignUp) {
    e.preventDefault();
    fetch('http://localhost:9999/auth/sign' + (isSignUp ? 'up' : 'in'), {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(rawData => rawData.json()).then(response => {
      if (response.username) {
        toast.success(response.message, {
          autoClose: 1000,
          closeButton: false,
          onClose: () => {
            this.setState({
              username: response.username,
              isAdmin: false
            });
            if (response.isAdmin) {
              this.setState({
                isAdmin: true
              });
            }
            localStorage.setItem('username', response.username);
            localStorage.setItem('isAdmin', response.isAdmin);
            if (window.location.href !== 'http://localhost:3000/checkout') {
              window.location = '/';
            }
          }
        });
      } else {
        let errorMsg;
        if (isSignUp) {
          errorMsg = response.errors['0'].msg;
        } else {
          errorMsg = response.message;
        }
        toast.error(errorMsg, {
          closeButton: false
        });
      }
    });
  }

  handleCreateProductSubmit(e, data) {
    e.preventDefault();
    if (!data.category) {
      toast.error('Please select product category', {
        closeButton: false
      });
    } else if (isNull(data.title) || data.title === "") {
      toast.error('Please enter product title', {
        closeButton: false
      });
    } else if (isNull(data.price) || data.price === "") {
      toast.error('Please enter product price', {
        closeButton: false
      });
    } else if (isNull(data.imageUrl) || data.imageUrl === "") {
      toast.error('Please enter product image URL', {
        closeButton: false
      });
    } else if (isNull(data.description) || data.description === "") {
      toast.error('Please enter product description', {
        closeButton: false
      });
    } else {
      fetch('http://localhost:9999/feed/product/create', {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(rawData => rawData.json()).then(response => {
        if (response.product) {
          toast.success(response.message, {
            autoClose: 1000,
            closeButton: false,
            onClose: () => {
              window.location = '/product/all';
            }
          });
        } else {
          toast.error(response.message, {
            closeButton: false
          })
        }
      });
    }
  }

  handleCreateCategorySubmit(e, data) {
    e.preventDefault();
    fetch('http://localhost:9999/feed/category/create', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(rawData => rawData.json()).then(response => {
      if (!response.errors) {
        if (response.error) {
          toast.error(response.error, {
            autoClose: 1000,
            closeButton: false
          });
        } else {
          toast.success(response.message, {
            autoClose: 1000,
            closeButton: false,
            onClose: () => {
              window.location = '/product/all';
            }
          });
        }
      } else {
        toast.error(response.message, {
          closeButton: false
        })
      }
    });
  }

  handleChange(e) {
    let isCheckBox = e.target.type === 'checkbox';
    let nutritionalInfo = e.target.id === 'nutritionalInfo';
    let selectCategory = e.target.id === 'selectCategory';

    if (isCheckBox) {
      const value = e.target.checked;
      let dataSheet = { ...this.state.dataSheet };
      dataSheet[e.target.name] = value;
      this.setState({
        dataSheet
      });
    } else if (nutritionalInfo) {
      let nutritionalInfo = { ...this.state.nutritionalInfo };
      nutritionalInfo[e.target.name] = +e.target.value;
      this.setState({
        nutritionalInfo
      });
    } else if (selectCategory) {
      let category = e.target.value;
      this.setState({
        category
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
  }

  handleEditChange(e) {
    let isCheckBox = e.target.type === 'checkbox';
    let nutritionalInfo = e.target.id === 'nutritionalInfo';
    let selectCategory = e.target.id === 'selectCategory';
    let product = { ...this.state.product };

    if (isCheckBox) {
      const value = e.target.checked;
      product.dataSheet[e.target.name] = value;
      this.setState({
        product
      });
    } else if (nutritionalInfo) {
      product.nutritionalInfo[e.target.name] = +e.target.value;
      this.setState({
        product
      });
    } else if (selectCategory) {
      let category = e.target.value;
      product.category = category;
      this.setState({
        product
      })
    } else {
      product[e.target.name] = e.target.value;
      this.setState({
        product
      });
    }
  }

  onLogout() {
    localStorage.clear();
    this.setState({
      user: null,
      isAdmin: false
    });
  }

  productHoverEvent(idx) {
    this.setState({
      selectedProductId: idx
    });
  }

  addToCart(product) {
    let cart = [...this.state.cart];
    product.quantity = 1;
    var index = cart.map(function (x) { return x._id; }).indexOf(product._id);
    if (index !== -1) {
      cart[index].quantity++;
    } else {
      cart.push(product);
    }

    toast.success('Product added to cart', {
      autoClose: 1000,
      closeButton: false,
      onClose: () => {
        localStorage.setItem('cart', JSON.stringify(cart));
        window.location.reload();
      }
    });
  }

  removeCartProduct(index) {
    let cart = [...this.state.cart];
    cart.splice(index, 1);
    localStorage.setItem('cart', JSON.stringify(cart));
    window.location.reload();
  }

  handleQuantityChange(e) {
    e.preventDefault();
    let cart = [...this.state.cart];
    cart[e.target.id].quantity = +e.target.value;
  }

  handleCart(cart) {
    localStorage.setItem('cart', JSON.stringify(cart));
  }

  clearCart(e) {
    e.preventDefault();
    let cart = JSON.parse(localStorage.getItem('cart'));
    let username = localStorage.getItem('username');
    let body = { cart, username };
    fetch('http://localhost:9999/auth/order', {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(rawData => rawData.json()).then(response => {
      if (!response.errors) {
        if (response.error) {
          toast.error(response.error, {
            autoClose: 1000,
            closeButton: false
          });
        } else {
          toast.success(response.message, {
            autoClose: 1000,
            closeButton: false,
            onClose: () => {
              window.location = '/complete-order';
            }
          });
        }
      } else {
        toast.error(response.message, {
          closeButton: false
        })
      }
    });
    localStorage.removeItem('cart');
  }

  orderProductsByDate() {
    let products = [...this.state.products];
    if (products) {
      let sortedProducts = products.sort(function (a, b) {
        var key1 = new Date(a.dateCreated);
        var key2 = new Date(b.dateCreated);

        if (key1 > key2) {
          return -1;
        } else if (key1 === key2) {
          return 0;
        } else {
          return 1;
        }
      });
      this.setState({
        products: sortedProducts
      });
    }
  }

  orderProductsByTitle() {
    let products = [...this.state.products];
    if (products) {
      let sortedProducts = products.sort((a, b) => {
        let key1 = a.title.toLowerCase();
        let key2 = b.title.toLowerCase();

        return (key1 < key2) ? -1 : (key1 > key2) ? 1 : 0;
      });
      this.setState({
        products: sortedProducts
      });
    }
  }

  orderProductsDesc() {
    let products = [...this.state.products];
    if (products) {
      let sortedProducts = products.sort((a, b) => {
        let key1 = a.title.toLowerCase();
        let key2 = b.title.toLowerCase();

        return (key1 > key2) ? -1 : (key1 < key2) ? 1 : 0;
      });
      this.setState({
        products: sortedProducts
      });
    }
  }

  orderProductsByPriceDesc() {
    let products = [...this.state.products];
    if (products) {
      let sortedProducts = products.sort((a, b) => {
        let key1 = a.price;
        let key2 = b.price;

        return (key1 > key2) ? -1 : (key1 < key2) ? 1 : 0;
      });
      this.setState({
        products: sortedProducts
      });
    }
  }

  orderProductsByPriceAsc() {
    let products = [...this.state.products];
    if (products) {
      let sortedProducts = products.sort((a, b) => {
        let key1 = a.price;
        let key2 = b.price;

        return (key1 < key2) ? -1 : (key1 > key2) ? 1 : 0;
      });
      this.setState({
        products: sortedProducts
      });
    }
  }

  handleEditProductSubmit(e, prop) {
    e.preventDefault();
    let data = prop.product
    if (!data.category) {
      toast.error('Please select product category', {
        closeButton: false
      });
      return;
    } else if (data.title === "") {
      toast.error('Please enter product title', {
        closeButton: false
      });
      return;
    } else if (data.price === "") {
      toast.error('Please enter product price', {
        closeButton: false
      });
      return;
    } else if (data.imageUrl === "") {
      toast.error('Please enter product image URL', {
        closeButton: false
      });
      return;
    } else if (data.description === "") {
      toast.error('Please enter product description', {
        closeButton: false
      });
      return;
    } else {
      fetch('http://localhost:9999/feed/edit/' + data._id, {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(rawData => rawData.json()).then(response => {
        if (!response.errors) {
          if (response.error) {
            toast.error(response.error, {
              autoClose: 1000,
              closeButton: false
            });
          } else {
            toast.success(response.message, {
              autoClose: 1000,
              closeButton: false,
              onClose: () => {
                window.location = '/product/all';
              }
            });
          }
        } else {
          toast.error(response.message, {
            closeButton: false
          })
        }
      });
    }
  }

  handleDeleteProductSubmit(e, prop) {
    e.preventDefault();
    let data = prop.product;
    fetch('http://localhost:9999/feed/remove/' + data._id, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(rawData => rawData.json()).then(response => {
      if (!response.errors) {
        if (response.error) {
          toast.error(response.error, {
            autoClose: 1000,
            closeButton: false
          });
        } else {
          toast.success(response.message, {
            autoClose: 1000,
            closeButton: false,
            onClose: () => {
              window.location = '/product/all';
            }
          });
        }
      } else {
        toast.error(response.message, {
          closeButton: false
        })
      }
    });
  }

  redirectToSocial(e) {
    e.preventDefault();
    if (e.target.id === 'twitter') {
      window.open('https://www.twitter.com', '_blank');
    } else if (e.target.id === 'facebook') {
      window.open('https://www.facebook.com', '_blank');
    } else if (e.target.id === 'linkedin') {
      window.open('https://www.linkedin.com', '_blank');
    } else if (e.target.id === 'dribble') {
      window.open('https://www.dribble.com', '_blank');
    }
  }

  handleSearchChange(e) {
    let search = e.target.value;
    if (search && search !== "") {
      this.setState({
        search
      });
    }
  }

  searchProducts(e) {
    e.preventDefault();
    if (this.state.search && this.state.search !== "") {
      window.location.href = "/search?product=" + this.state.search;
      localStorage.setItem('search', this.state.search);
    } else {
      window.location.href = "/product/all"
    }
  }

  submitContactForm(e) {
    e.preventDefault();
    toast.success("Sent successfully", {
      autoClose: 1000,
      closeButton: false,
      onClose: () => {
        window.location = '/';
      }
    });
  }

  render() {
    return (
      <div className="App" >
        <ToastContainer />
        <Header isAdmin={this.state.isAdmin} username={this.state.username} onLogout={this.onLogout} cart={this.state.cart} categories={this.state.categories} handleSearchChange={this.handleSearchChange.bind(this)} searchProducts={this.searchProducts.bind(this)} />
        <Switch>
          <Route exact path="/" render={(props) =>
            <Home
              {...props}
              isAdmin={this.state.isAdmin}
              categories={this.state.categories}
              products={this.state.products}
              cart={this.state.cart}
              productHoverEvent={this.productHoverEvent.bind(this)}
            />} />
          <Route exact path="/cart" render={(props) =>
            <Cart
              {...props}
              cart={this.state.cart}
              removeCartProduct={this.removeCartProduct.bind(this)}
              handleQuantityChange={this.handleQuantityChange.bind(this)}
              handleCart={this.handleCart.bind(this)}
            />}
          />
          <Route exact path="/checkout" render={(props) =>
            this.state.username ?
              <Checkout
                {...props}
                cart={this.state.cart}
                clearCart={this.clearCart.bind(this)}
              />
              :
              <Login
                message={'In order to checkout, please login or sign up.'}
                handleSubmit={this.handleSubmit.bind(this)}
                handleChange={this.handleChange}
              />
          }
          />
          <Route exact path="/complete-order" render={(props) =>
            <Order
              {...props}
            />}
          />
          <Route exact path="/orders" render={(props) =>
            <Orders
              {...props}
            />}
          />
          <Route exact path="/product/all" render={(props) =>
            <ProductList
              {...props}
              isAdmin={this.state.isAdmin}
              products={this.state.products}
              orderProductsByDate={this.orderProductsByDate.bind(this)}
              orderProductsByTitle={this.orderProductsByTitle.bind(this)}
              orderProductsDesc={this.orderProductsDesc.bind(this)}
              orderProductsByPriceAsc={this.orderProductsByPriceAsc.bind(this)}
              orderProductsByPriceDesc={this.orderProductsByPriceDesc.bind(this)}
            />}
          />
          <Route exact path="/category/filter/:link" render={(props) =>
            <ProductList
              {...props}
              isAdmin={this.state.isAdmin}
              products={this.state.filterProducts}
            />}
          />
          <Route path={"/search"} render={(props) =>
            <ProductList
              {...props}
              isAdmin={this.state.isAdmin}
              search={this.state.search}
              products={this.state.products.filter(product => product.title.toLocaleLowerCase().includes(localStorage.getItem('search').toLocaleLowerCase()))}
              orderProductsByDate={this.orderProductsByDate.bind(this)}
              orderProductsByTitle={this.orderProductsByTitle.bind(this)}
              orderProductsDesc={this.orderProductsDesc.bind(this)}
              orderProductsByPriceAsc={this.orderProductsByPriceAsc.bind(this)}
              orderProductsByPriceDesc={this.orderProductsByPriceDesc.bind(this)}
            />} />
          <Route exact path="/details/:id"
            render={(props) =>
              <Details
                {...props}
                addToCart={this.addToCart}
                product={this.state.products.find(p => p._id === this.state.selectedProductIdx)}
              />}
          />
          <Route exact path="/product/add" render={(props) =>
            this.state.isAdmin ?
              <CreateProduct
                {...props}
                categories={this.state.categories}
                handleCreateProductSubmit={this.handleCreateProductSubmit.bind(this)}
                handleChange={this.handleChange}
              /> :
              <Redirect to={{
                pathname: "/login"
              }} />
          } />
          <Route exact path="/edit/:id" render={(props) =>
            this.state.isAdmin ?
              <EditProduct
                {...props}
                products={this.state.products}
                categories={this.state.categories}
                handleEditChange={this.handleEditChange}
                handleEditProductSubmit={this.handleEditProductSubmit.bind(this)}
              /> :
              <Redirect to={{
                pathname: "/login"
              }} />
          } />
          <Route exact path="/remove/:id" render={(props) =>
            this.state.isAdmin ?
              <DeleteProduct
                {...props}
                products={this.state.products}
                categories={this.state.categories}
                handleDeleteProductSubmit={this.handleDeleteProductSubmit.bind(this)}
              /> :
              <Redirect to={{
                pathname: "/login"
              }} />
          } />
          <Route exact path="/category/add" render={(props) =>
            this.state.isAdmin ?
              <Category
                {...props}
                handleCreateCategorySubmit={this.handleCreateCategorySubmit.bind(this)}
                handleChange={this.handleChange}
              /> :
              <Redirect to={{
                pathname: "/login"
              }} />
          } />
          <Route exact path="/login" render={(props) =>
            this.state.username ?
              <Redirect to={{
                pathname: "/"
              }} />
              :
              <Login
                {...props}
                handleSubmit={this.handleSubmit.bind(this)}
                handleChange={this.handleChange}
              />
          } />
          <Route exact path="/register" render={(props) =>
            this.state.username ?
              <Redirect to={{
                pathname: "/"
              }} />
              :
              <Register
                {...props}
                handleSubmit={this.handleSubmit.bind(this)}
                handleChange={this.handleChange}
              />
          } />
          <Route exact path="/about" render={() => <About />} />
          <Route exact path="/returns" render={() => <Returns />} />
          <Route exact path="/delivery-info" render={() => <Delivery />} />
          <Route exact path="/privacy-policy" render={() => <PrivacyPolicy />} />
          <Route exact path="/terms-of-use" render={() => <Terms />} />
          <Route exact path="/blog" render={() => <Blog />} />
          <Route exact path="/contact" render={() => <Contact submitContactForm={this.submitContactForm.bind(this)} />} />
          <Route render={() => <h1 align="center">Not found!</h1>}></Route>
        </Switch>
        <Footer redirectToSocial={this.redirectToSocial.bind(this)} />
        <div className="gototop js-top">
          <Link to="#" className="js-gotop"><i className="ion-ios-arrow-up" /></Link>
        </div>
      </div>
    );
  }
}

export default App;
