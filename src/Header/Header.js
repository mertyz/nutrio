import React, { Component } from 'react';

class Header extends Component {
  render() {
    const { onLogout } = this.props;
    return (
      <nav className="colorlib-nav" role="navigation">
        {
          this.props.username ?
            <div className="col-sm-11 text-right menu-1">
              <ul><li className="cart"><a href="/orders">Welcome, {this.props.username}</a></li></ul>
            </div>
            :
            null
        }
        <div className="top-menu">
          <div className="container">
            <div className="row">
              <div className="col-sm-7 col-md-9">
                <div id="colorlib-logo"><a href="/">Nutrio</ a></div>
              </div>
              <div className="col-sm-5 col-md-3">
                <form onSubmit={(e) => this.props.searchProducts(e)} className="search-wrap">
                  <div className="form-group">
                    <input type="search" className="form-control search" placeholder="Search" name="search" onChange={this.props.handleSearchChange}/>
                    <button className="btn btn-primary submit-search text-center" type="submit"><i className="icon-search" /></button>
                  </div>
                </form>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 text-left menu-1">
                <ul>
                  <li><a href="/">Home</a></li>
                  <li className="has-dropdown">
                    <a href="/product/all">Products</a>
                    <ul className="dropdown">
                      {
                        this.props.categories.map((category, index) => {
                          return (
                            <li key={index}><a href={`/category/filter/${category.link}`}>{category.title}</a></li>
                          )
                        })
                      }
                    </ul>
                  </li>
                  <li><a href="/about">About</a></li>
                  <li><a href="/contact">Contact</a></li>
                  <li className="cart"><a href="/cart"><i className="icon-shopping-cart" /> Cart [{this.props.cart ? this.props.cart.length : 0}]
                    </a>
                  </li>
                  {
                    this.props.username ?
                      (<span>
                        <li className="cart"><a href="/" onClick={onLogout}>Logout</a></li>
                        {this.props.isAdmin ? (
                          <span>
                            <li className="cart"><a href="/category/add">Add Category</a></li>
                            <li className="cart"><a href="/product/add">Add Product</a></li>
                          </span>
                        )
                          :
                          null}
                      </span>)
                      :
                      (<span>
                        <li className="cart"><a href="/register">Register</a></li>
                        <li className="cart"><a href="/login">Login</a></li>
                      </span>)
                  }
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="sale">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 offset-sm-2 text-center">
                <div className="row">
                  <div className="owl-carousel2">
                    <div className="item">
                      <div className="col">
                        <h3><a href="/">25% off (Almost) Everything! Use Code: Summer Sale</a></h3>
                      </div>
                    </div>
                    <div className="item">
                      <div className="col">
                        <h3><a href="/">Our biggest sale yet 50% off all beauty and higiene products</a></h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;