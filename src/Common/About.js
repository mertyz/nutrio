/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class About extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>About</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-about">
                    <div className="container">
                        <div className="row row-pb-lg">
                            <div className="col-sm-6 mb-3">
                                <div className="video colorlib-video" style={{ backgroundImage: 'url(images/about.jpg)' }}>
                                    <a href="https://www.youtube.com/watch?v=dzLAzDoVi7U" className="popup-vimeo"><i className="icon-play3" /></a>
                                    <div className="overlay" />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="about-wrap">
                                    <h2>Nutrio®</h2>
                                    <h2>Good Health Made Simple™</h2>
                                    <p align="justify">Good health does not need to be complicated. We understand the complex needs of a healthy lifestyle and develop supplements which work synergistically with your natural physiology to help promote proper balance and wellness. Nutrio® has good health covered. From immune support to digestive health along with vitamins and specialty supplements, you will be sure to find simple solutions for everyday optimum health.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default About;