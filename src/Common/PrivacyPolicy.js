/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class PrivacyPolicy extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>Privacy Policy</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-about">
                    <div className="container">
                        <div className="row row-pb-lg">
                            <div className="col-sm-12">
                                <div className="about-wrap">
                                    <div id="center_column" className="center_column col-xs-12 col-sm-12">
                                        <div className="rte container recipe">
                                            <h1 className="recipe-title" align="center">Privacy Policy</h1>
                                            <p><span>The access to the website </span><span><a href="/"><span>www.nutrio.com</span></a></span><span> and its use are subject to the present general conditions. The client is expected to read them carefully before ordering.</span></p>
                                            <h2><strong><span>Identification of the service provider Nutrio&nbsp;</span></strong></h2>
                                            <p><span>Nutrio sprl</span></p>
                                            <p><span>Address&nbsp;:<br />291 South 21th Street,<br />Suite 721 New York NY 10016<br /> USA</span></p>
                                            <p><span>Entreprise number: 0652941840 <br /> VAT Number : 652941840<br /> Phone&nbsp;:&nbsp;<span>+1235 2355 98</span><br /> Email&nbsp;: hello@nutrio.com</span></p>
                                            <h2><strong><span>Object</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>Nutrio offers products online. The customer visits the website and fills his basket with the products that he wishes to acquire. The customer will then accept the general conditions and proceed to the payment of his order via the online payment solutions available on the website. Afterwards, the customer will confirm his order. Nutrio will then send him the confirmation email of his order. The sales contract is concluded at the reception of the confirmation email.</span></p><p style={{ textAlign: 'justify' }}><span>Nutrio reserves the right of asking complementary information to the customer and refusing certain orders. Certain examples include: a minor client, litigations, inventories that do not allow order deliveries or an eventual obvious error in the product’s description or price. This list is not exhaustive. Only legally capable people can place order on the website.</span></p>
                                            <h2><strong><span>Application and acceptation of the general conditions</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>An order implies, in every case, that the customer expressively and irrevocably accepts the present general conditions. He will receive in the confirmation email of his order a link towards the general conditions in a format that can be saved or printed. Certain dispositions of the general conditions only apply to the consumers.</span></p>
                                            <h2><strong><span>Arrangement fees for delivery and payment</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>The delivery fees are indicated in the order summary on the website before the customer confirms his order. These fees are also indicated in the confirmation email. Nutrio is committed to do everything it can to make sure the delivery happens in maximum 4 working days in Belgium and maximum 8 working days in the rest of Europe at the address indicated by the customer or at the collection point chosen. For security reason, the customer can be invited to show a proof of his identity at the delivery. In case of a first absence at the delivery, a new presentation will take place. In case of a second absence, the package will be left at the closest collection point. No compensation can be claimed by the customer in case of late delivery.</span></p><p style={{ textAlign: 'justify' }}><span>At the delivery, the client verifies and agrees upon the information and data indicated on the bill. He also confirms that products are delivered and, as long as it is visible, comply with the description that Nutrio gave and that they are adapted for the usual usage of such products. Any defect should be notified to the transporter or the collection point. If certain products do not comply at the delivery, the customer must also inform immediately Nutrio client service.</span></p>
                                            <h2><strong><span>Price and website information</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>All the prices mentioned on the website include VAT and other taxes. These prices can be modified at any moment and do not include, in such case, delivery fees. The prices paid by the customer are those mentioned in the confirmation email, unless there is an obvious mistake.</span></p><p style={{ textAlign: 'justify' }}><span>The pictures and other illustrations that present the products have no binding force and can picture products or elements not included in the indicated price.</span></p><h2 style={{ textAlign: 'justify' }}><strong><span>Right to retract only for consumers and return fees </span></strong></h2><p style={{ textAlign: 'justify' }}><span>The customer must retract from the contract within a timeframe of 14 days from the reception of his or her order without penalties nor expression of motives. The customer can introduce a demand either in contacting the client service or by sending back the retraction form (<a href="http://www.kazidomi.com/img/cms/Appendix.pdf">see appendix</a>) filled in by mail or email, or by sending a clear demand in this sense. He or she can ask to exchange the products or be reimbursed.</span></p><p style={{ textAlign: 'justify' }}><span>During the retraction period, the customer will take great care of the products and their packaging. The customer will neither unpack nor use the products to the extend of the necessary to judge if he or she wishes to keep them or not. If the customer wishes to use his or her retraction right, the customer will send back the products with all the possible accessories (notices, documentations, etc.), and, if possible, in their state and packaging of origin, at the following address: 291 South 21th Street,
                                            Suite 721 New York NY 10016. The return shipment must happen in a maximum delay of 14 days from the day of the exercise of the retraction right. </span></p><p style={{ textAlign: 'justify' }}><span>Nutrio will reimburse the customer with the help of the same payment mode as the one used by the customer to make the order. The reimbursement will be made, deducting the delivery fees paid by the client or supported by Nutrio. The reimbursement will happen within the 14 days following the acknowledgment of the retraction demand to Nutrio, provided that Nutrio has received the products returned or that the customer has proven their expedition. </span></p><p style={{ textAlign: 'justify' }}><span>The responsibility of the customer will be engaged for any manipulation of the products other than the ones necessary to establish their nature, characteristics and well functioning of the products. </span></p><h2 style={{ textAlign: 'justify' }}><strong><span>Products conformity</span></strong></h2><p style={{ textAlign: 'justify' }}><span>Nutrio guarantees that products comply to the contract and to the specificities described on the website and to the reasonable requirements to their quality. In the case of an obvious vice or that an unconformity is identified the customer must imperatively and immediately notify Nutrio, under penalty of the loss of his or her right with this regards, without prejudices of the rights given by the articles 1649bis and following of the civil code if the customer is a consumer and by the article 1641 and following if he or she is not a consumer. In the second case, it is supposed that Nutrio did not know about the hidden vice. </span></p><h2 style={{ textAlign: 'justify' }}><strong><span>Proof</span></strong></h2>
                                            <p><span>Emails and automatic registering systems of the website are used as proves, for instance, for the order dates and order content. </span></p>
                                            <h2><strong><span>Client service and possible reclamations</span></strong></h2>
                                            <p><span>Our slient service will do everything it can to answer your questions, suggestions or complaints and help you the best it can. It can be reached: </span></p>
                                            <p><span>- By the contact page on the website</span></p>
                                            <p><span>- By email&nbsp;: hello@nutrio.com</span></p>
                                            <p><span>- By phone&nbsp;:&nbsp;<span>0032 2 479 82 62</span></span></p>
                                            <p><span>- By mail: Nutrio, Client Service, 1291 South 21th Street, Suite 721 New York NY 10016</span></p>
                                            <h2><strong><span>Responsibility</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>Nutrio cannot be held responsible for a wrong use of the products or for potential modifications of the products realized by the fabricant. The responsibility of Nutrio will, in any case, be limited to the amount of the order and cannot be engaged for simple error that could have existed despite the attention brought to the products' presentation. Nutrio will never be responsible for indirect damage or following neither for a damage that was not foreseeable at the moment of the order. Nor the pictures, illustrations nor the products' documentations engage Nutrio. This is only indicative information. The customer engages to read carefully the product notice given by the fabricants on or in the product.</span></p>
                                            <h2><strong><span>Personal data</span></strong></h2>
                                            <p><span>Nutrio respects Belgium law 8th December 1992 as revised on the personal data treatment. The customer has a right to access, modification, rectification and suppression of the data that concerns him or her by contacting Nutrio.</span></p>
                                            <p><span>Nutrio engages not to communicate the contact details about its clients to third parties without his or her consent.</span></p>
                                            <h2><strong><span>Intellectual rights</span></strong></h2>
                                            <p style={{ textAlign: 'justify' }}><span>The name and logo of Nutrio as those of the enterprises indicated on the website are names and brands protected. The intellectual rights linked to the website are the exclusive intellectual property of Nutrio or of its co-contractors. The information on the website cannot be made public, reproduced or modified without the prior written consent of Nutrio, excepted for strictly personal use.</span></p><h2 style={{ textAlign: 'justify' }}><strong><span>Diverse</span></strong></h2><p style={{ textAlign: 'justify' }}><span>The fact that one provision of the present general conditions is declared invalid, illegal, void or null does not undermine the validity, legality or applicability of the rest of the provisions. The fact that Nutrio omits to require the strict application of one or several dispositions in the general conditions does not constitute a tacit waiver to its right and does not prevent it to subsequently require strict compliance with these provisions.</span></p><p style={{ textAlign: 'justify' }}><span>Nutrio is not responsible for the late execution nor the non-execution of the Service in case of act of God. Is considered as an act of God the following</span><span> hypotheses usually recognized by the jurisprudence Belgian courts. In case of act of Gods, Nutrio is allowed to delivery the products within 60 days following the contractual date. After the delay, the customer can terminate his or her order. </span></p>
                                            <p><span>Any litigation will be ruled by the Belgian law. Only Belgian courts are qualified.</span></p>
                                            <p><span>You can notify your litigation on the European platform ODR: </span><span><a href="http://ec.europa.eu/consumers/odr/"><span>http://ec.europa.eu/consumers/odr/</span></a></span><span>. </span></p>
                                            <p><span>&nbsp;</span></p>
                                            <p><span>&nbsp;</span></p>
                                            <h2><strong><span>Appendix: retraction forms</span></strong></h2>
                                            <p><span>Fill in and send this form only if you wish to terminate your contract: </span></p>
                                            <p><span>I notify you, with the present form, my contract termination on the sale of the following product(s):</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Order made on the ………………………………. (Please fill in the date)</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Order received on the ……………………………… (Please fill in the date)</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Name of the person who ordered: ……………………………………………..</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Address communicated at the order:…………………………………………………………..</span></p>
                                            <p><span>……………………………………………………………………………………………………………………………………</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Date: …………………………………………………………</span></p>
                                            <p><span>-<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span>Signature (Only if this form is given in paper version)&nbsp;:</span></p>
                                            <p><span>&nbsp;</span></p>
                                            <p><span>……………………………………………………………………</span></p>
                                            <p><span>&nbsp;</span></p>
                                            <p><span>&nbsp;</span></p>
                                        </div> <br />
                                        <div className="footer-cookbook container" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PrivacyPolicy;