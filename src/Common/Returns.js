/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class Returns extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>Returns and Exchange</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12">
                    <div className="about-wrap">
                        <h2>Right to retract only for consumers and return fees</h2>
                        <p align="justify">The customer must retract from the contract within a timeframe of 14 days from the reception of his or her order without penalties nor expression of motives. The customer can introduce a demand either in contacting the client service or by sending back the retraction form (see appendix) filled in by mail or email, or by sending a clear demand in this sense. He or she can ask to exchange the products or be reimbursed.
                        <br />
                            <br />
                            During the retraction period, the customer will take great care of the products and their packaging. The customer will neither unpack nor use the products to the extend of the necessary to judge if he or she wishes to keep them or not. If the customer wishes to use his or her retraction right, the customer will send back the products with all the possible accessories (notices, documentations, etc.), and, if possible, in their state and packaging of origin, at the following address: 291 South 21th Street, Suite 721 New York NY 10016. The return shipment must happen in a maximum delay of 14 days from the day of the exercise of the retraction right.
                        <br />
                            <br />
                            Nutrio will reimburse the customer with the help of the same payment mode as the one used by the customer to make the order. The reimbursement will be made, deducting the delivery fees paid by the client or supported by Nutrio. The reimbursement will happen within the 14 days following the acknowledgment of the retraction demand to Nutrio, provided that Nutrio has received the products returned or that the customer has proven their expedition.
                        <br />
                            <br />
                            The responsibility of the customer will be engaged for any manipulation of the products other than the ones necessary to establish their nature, characteristics and well functioning of the products.</p>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Returns;