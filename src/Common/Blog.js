/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class Blog extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>Blog</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-about">
                    <div className="container">
                        <div className="row row-pb-lg">
                            {/* Section: Blog v.4 */}
                            <section className="my-5">
                                <h1 align="center">Recent posts</h1>
                                <br />
                                <br />
                                {/* Grid row */}
                                <div className="row">
                                    {/* Grid column */}
                                    <div className="col-md-12">
                                        {/* Card */}
                                        <div className="card card-cascade wider reverse">
                                            {/* Card image */}
                                            <div className="view view-cascade overlay">
                                                <img className="card-img-top" src="https://32lxcujgg9-flywheel.netdna-ssl.com/wp-content/uploads/2019/03/sweet_potato_pasta.jpg" alt="Sample image" />
                                                <a href="#!">
                                                    <div className="mask rgba-white-slight" />
                                                </a>
                                            </div>
                                            {/* Card content */}
                                            <div className="card-body card-body-cascade text-center">
                                                {/* Title */}
                                                <h2 className="font-weight-bold"><a>SWEET POTATO PASTA WITH VEGAN WALNUT CREAM SAUCE</a></h2>
                                                {/* Data */}
                                                <p>Written by <a><strong>Abby Madison</strong></a>, 26/08/2018</p>
                                                {/* Social shares */}
                                                <div className="social-counters">
                                                    {/* Facebook */}
                                                    <a className="btn btn-fb">
                                                        <i className="fab fa-facebook-f pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Facebook</span>
                                                    </a>
                                                    <span className="counter">46</span>
                                                    {/* Twitter */}
                                                    <a className="btn btn-tw">
                                                        <i className="fab fa-twitter pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Twitter</span>
                                                    </a>
                                                    <span className="counter">22</span>
                                                    {/* Google+ */}
                                                    <a className="btn btn-gplus">
                                                        <i className="fab fa-google-plus-g pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Google+</span>
                                                    </a>
                                                    <span className="counter">31</span>
                                                    {/* Comments */}
                                                    <a className="btn btn-default">
                                                        <i className="far fa-comments pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Comments</span>
                                                    </a>
                                                    <span className="counter">18</span>
                                                </div>
                                                {/* Social shares */}
                                            </div>
                                            {/* Card content */}
                                        </div>
                                        {/* Card */}
                                        {/* Excerpt */}
                                        <div className="mt-5" style={{ textAlign: "justify" }}>
                                            As we wait patiently (or not so patiently) for spring, I’m sharing what feels like a bit of a last-hurrah for winter recipes. This pasta with walnut cream sauce came out of one of my instagram recipes. People liked it so much, I felt like it deserved a place on the site. It’s rich sauce is vegan, but I think that makes the creaminess even more amazing.
                                            <br />
                                            <br />
                                            <h2>Walnut Cream, the real star</h2>
                                            <p>
                                                In all the nut-based alternative creams/cheeses, walnut cream reigns supreme in my life. I love the warm flavor and how nicely it purees into a smooth sauce. The softness of the walnuts is also your friend here. I find the walnuts only need a good hot soak for about an hour.</p>
                                            <p>Of course, if you’re not on the walnut-wagon, you can use a more milder cashew cream or even an almond cream.</p>
                                            <br />
                                            <h2>Sweet potatoes (and other veg)</h2>
                                            <p>
                                                I realize that as I’m posting this recipe, you’re probably over sweet potatoes and ready for spring. I’m with you, but I have a few more to use before I dive head first into spring. Swap the sweet potatoes for winter squash or try it with some steamed greens like kale or chard.</p>
                                            <br />
                                            <h2>Grain Bowls</h2>
                                            <p>
                                                Finally, if pasta isn’t your jam, you could turn this into a delicious grain bowl. I’d plan to cut the sauce in half and use more as a drizzle and less as a sauce. Use a hearty grain here, like sorghum or wheat berries.</p>
                                        </div>
                                    </div>
                                    {/* Grid column */}
                                </div>
                                {/* Grid row */}
                                <hr className="mb-5 mt-4" />
                                {/* Grid row */}
                                <div className="row">
                                    {/* Grid column */}
                                    <div className="col-md-12">
                                        {/* Card */}
                                        <div className="card card-cascade wider reverse">
                                            {/* Card image */}
                                            <div className="view view-cascade overlay">
                                                <img className="card-img-top" src="https://32lxcujgg9-flywheel.netdna-ssl.com/wp-content/uploads/2019/03/green_omelette.jpg" alt="Sample image" />
                                                <a href="#!">
                                                    <div className="mask rgba-white-slight" />
                                                </a>
                                            </div>
                                            {/* Card content */}
                                            <div className="card-body card-body-cascade text-center">
                                                {/* Title */}
                                                <h2 className="font-weight-bold"><a>HERBY KALE OMELETTE WITH MANCHEGO CHEESE</a></h2>
                                                {/* Data */}
                                                <p>Written by <a><strong>Abby Madison</strong></a>, 21/08/2018</p>
                                                {/* Social shares */}
                                                <div className="social-counters">
                                                    {/* Facebook */}
                                                    <a className="btn btn-fb">
                                                        <i className="fab fa-facebook-f pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Facebook</span>
                                                    </a>
                                                    <span className="counter">87</span>
                                                    {/* Twitter */}
                                                    <a className="btn btn-tw">
                                                        <i className="fab fa-twitter pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Twitter</span>
                                                    </a>
                                                    <span className="counter">73</span>
                                                    {/* Google+ */}
                                                    <a className="btn btn-gplus">
                                                        <i className="fab fa-google-plus-g pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Google+</span>
                                                    </a>
                                                    <span className="counter">91</span>
                                                    {/* Comments */}
                                                    <a className="btn btn-default">
                                                        <i className="far fa-comments pr-2" />
                                                        <span className="clearfix d-none d-md-inline-block">Comments</span>
                                                    </a>
                                                    <span className="counter">67</span>
                                                </div>
                                                {/* Social shares */}
                                            </div>
                                            {/* Card content */}
                                        </div>
                                        {/* Card */}
                                        {/* Excerpt */}
                                        <div className="mt-5" style={{ textAlign: "justify" }}>
                                            <h2>Green Sauce</h2>
                                            <p>
                                            When it comes to experimenting with recipes, sauces are up there as a favorite. It can be easy to completely shift a dish (or many dishes) with one simple sauce. I don’t use a lot of kale outside of a few recipes but using it in sauces ensures I use it all before it goes bad.</p>
                                            <br />
                                            <h2>SAUCY KALE OMELETTE</h2>
                                            <p>
                                            You can swap the kale for other greens such as spinach, chard, or collards. If you’re using spinach, drop the time for blanching. Spinach takes much less time: usually around 30 to 60 seconds.</p>
                                            <br />
                                            <h2>TARRAGON</h2>
                                            <p>
                                            I know tarragon isn’t everyone’s favorite herb. You could swap it out for chives or if it’s summer, use fresh basil.</p>
                                            <br />
                                            <h2>Omelette vs Frittata</h2>
                                            <p>
                                            I love a solid omelette for my morning breakfast but if I’m making a dish to feed the family, I usually stick with frittata. You can easily use this same concept in frittata form. I like to use this base recipe and right before I transfer the pan to the oven, I swirl in the kale sauce.</p>
                                            <br />
                                            <h2>Add some Grains</h2>
                                            <p>Leftover grains? Add a few to the omelette. I really like using cooked grains in the omelette or as an omelette filling. This also works if you’re making the frittata (as mentioned in the previous paragraph!)</p>
                                            <br />
                                            <h2>Veg Bulk</h2>
                                            <p>Depending on the time of year, add fresh or cooked vegetables to the filling. During the cooler months, roasted squash or sweet potatoes. For spring, try some pan-fried asparagus then during summer, blistered tomatoes!</p>
                                            <br />
                                            <h2>Vegan-it</h2>
                                            <p>Finally, since the sauce is vegan, I’d be remiss if I didn’t add a vegan option for the omelette. Use the kale sauce in a tofu scramble, make a grain bowl, or there’s the new ‘just’ product that uses mung beans as a base.</p>
                                        </div>
                                    </div>
                                    {/* Grid column */}
                                </div>
                                {/* Grid row */}
                                <hr className="mb-5 mt-4" />
                            </section>
                            {/* Section: Blog v.4 */}
                        </div>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div >

        );
    }
}

export default Blog;