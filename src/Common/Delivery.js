/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class Delivery extends Component {
    render() {
        return (
            <div>
                <div className="breadcrumbs">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p className="bread"><span><a href="index.html">Home</a></span> / <span>Delivery Information</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12">
                    <div className="about-wrap">
                        <div className><div id="center_column" className="center_column col-xs-12 col-sm-12"><div className="rte container recipe"><h1 className="recipe-title" align="center">Delivery Information</h1><h2>Prices</h2><p><span>Shipping fees are offered above €49 in&nbsp;<br />Belgium<br />France<br />Germany<br />Austria<br />Holland<br />Italy<br />Luxemburg<br />United Kingdom<br /><br />Shipping fees are offered above €69 in&nbsp;<br />Bulgaria<br />Croatia<br />Finland<br />Hungary<br />Irland<br />Lituania<br />Poland<br />Portugal<br />Czech republic<br />Slovenia<br />Sweden<br /><br />Shipping fees are offered above €100 in&nbsp;<br />Switzerland<br />Spain<br /><br /></span></p><p><span><br /></span><span>The fee schedule and time is as follows:</span></p><table border={3} width={539}><tbody><tr><td>Country</td><td>Delivery method</td><td>Price</td><td>Time</td></tr><tr><td>Belgium</td><td>Pickup point</td><td>5€</td><td>D + 2</td></tr><tr><td>Belgium</td><td>Locker</td><td>5€</td><td>D + 2</td></tr><tr><td>Belgium</td><td>Home</td><td>5€</td><td>D + 2</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Luxembourg</td><td>Home</td><td>5€</td><td>D + 2</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Holland</td><td>Home</td><td>5€</td><td>D + 2</td></tr><tr><td /><td /><td /><td /></tr><tr><td>France</td><td>Home</td><td>5€</td><td>D + 3</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Germany</td><td>Home</td><td>5€</td><td>D + 2/3</td></tr><tr><td /><td /><td /><td /></tr><tr><td>United Kingdom</td><td>Home</td><td>5€</td><td>D + 3/4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Spain</td><td>Home</td><td>5€</td><td>D + 4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Denmark</td><td>Home</td><td>5€</td><td>D +4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Italy</td><td>Home</td><td>5€</td><td>D +4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Switzerland</td><td>Home</td><td>5€</td><td>D +4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Austria</td><td>Home&nbsp;</td><td>5€</td><td>D +4</td></tr><tr><td /><td /><td /><td /></tr><tr><td>Portugal</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Bulgaria</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Croatia</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Finland</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Lituania</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Czech republic</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Sweden</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Poland</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Hongary</td><td>Home</td><td>5€</td><td><span>D +4</span></td></tr><tr><td /><td /><td /><td /></tr><tr><td>Irland</td><td>Home</td><td>5€</td><td>D +4</td></tr></tbody></table><br /><h2><strong><span>Expeditions</span></strong></h2><p><span>Packages are generally dispatched within 1-2 days after receipt of payment. They are shipped via the Post with a tracking number. You can also benefit from a package repository collection point or locker. Whatever the delivery method chosen, we send you a link to track your package online.</span></p><p><span>Shipping charges include preparation and packing costs and postage. Preparation costs are fixed, whereas transport fees vary. We recommend you to group all your items in one order. We can combine two orders placed separately, and shipping charges apply to each of them. Your package is shipped at your own risk, but special attention is paid to fragile items.</span><span><br /><br /></span><span>Boxes dimensions are appropriate and your items are properly protected.</span></p>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="colorlib-partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
                                <h2>Trusted Partners</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col partner-col text-center">
                                <img src="images/logo1.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo2.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo3.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo4.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                            <div className="col partner-col text-center">
                                <img src="images/logo5.png" className="img-fluid" alt="Free html4 bootstrap 4 template" />
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Delivery;