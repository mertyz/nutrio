const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
  title: {
    type: String,
    required: true
  },
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
  }]
}, {
  getters: true
});

categorySchema.set("toJSON", { getters: true, virtuals: true });
categorySchema.set("toObject", { virtuals: true });

categorySchema.virtual('link').get(function () {
  return this.link = this.title.toLowerCase().split(' ').join('-');
});

module.exports = mongoose.model('Category', categorySchema);