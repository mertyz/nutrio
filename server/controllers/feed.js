const Product = require('../models/Product');
const Category = require('../models/Category');

module.exports = {
  getProducts: (req, res, next) => {
    Product.find()
      .then((products) => {
        res
          .status(200)
          .json({ message: 'Fetched products successfully.', products });
      })
      .catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      });
  },
  getProductById: (req, res, next) => {
    Product.findById(req.params.id)
      .then((product) => {
        res
          .status(200)
          .json({ message: 'Fetched product successfully.', product });
      })
      .catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      });
  },
  createProduct: (req, res, next) => {
    const productObj = req.body;
    Product.create(productObj)
      .then((product) => {
        Category.findById(product.category)
          .then((category) => {
            category.products.push(product._id);
            category.save();
            res.status(200)
              .json({
                message: 'Product created successfully!',
                product
              })
          }).catch((error) => {
            if (!error.statusCode) {
              error.statusCode = 500;
            }
            next(error);
          });
      })
      .catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      });
  },
  getCategories: (req, res, next) => {
    Category.find()
      .then((categories) => {
        res
          .status(200)
          .json({ message: 'Fetched categories successfully.', categories });
      })
      .catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      });
  },
  getCategoryById: (req, res, next) => {
    Category.findById(req.params.id)
      .then((category) => {
        res
          .status(200)
          .json({ message: 'Fetched product successfully.', category });
      })
      .catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      });
  },
  getCategoryProducts: (req, res, next) => {
    let id;
    Category.find()
      .then((categories) => {
        for (const category of categories) {
          if (category.link === req.params.link) {
            id = category._id;
          }
        }
        Category.findById(id).populate('products')
          .then((category) => {
            res
              .status(200)
              .json({ message: 'Fetched product successfully.', category });
          })
      }).catch((error) => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        next(error);
      }).catch((e) => {
        if (!e.statusCode) {
          e.statusCode = 500;
        }
        next(e);
      });
  },
  postEditProduct: async (req, res, next) => {
    let id = req.params.id;
    Product.findById(id).then((product) => {
      Category.findById(product.category).then(category => {
        let productIndex = category.products.indexOf(product._id);
        category.products.splice(productIndex, 1);
        category.save().then(() => {
          product.title = req.body.title;
          product.category = req.body.category;
          product.description = req.body.description;
          product.price = req.body.price;
          product.nutritionalInfo = req.body.nutritionalInfo;
          product.dataSheet = req.body.dataSheet;
          Category.findById(product.category).then(c => {
            c.products.push(product._id);
            c.save();
            product.save();
            res
              .status(200)
              .json({ message: 'Edited product successfullly.', product });
          }).catch(e => {
            if (!e.statusCode) {
              e.statusCode = 500;
            }
            next(e);
          });
        });
      }).catch(e => {
        if (!e.statusCode) {
          e.statusCode = 500;
        }
        next(e);
      });

    }).catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
  },
  postDeleteProduct: (req, res, next) => {
    let id = req.params.id;
    let categoryId = req.body.category;
    Product.findByIdAndRemove(id).then((products) => {
      Category.findById(categoryId).then(category => {
        let productIndex = category.products.indexOf(id);
        category.products.splice(productIndex, 1);
        category.save();
        res
          .status(200)
          .json({ message: 'Deleted product successfullly.', products });
      }).catch(e => {
        if (!e.statusCode) {
          e.statusCode = 500;
        }
        next(e);
      });
    }).catch(e => {
      if (!e.statusCode) {
        e.statusCode = 500;
      }
      next(e);
    });
  },
  createCategory: async (req, res, next) => {
    const categoryObj = req.body;
    let category = await Category.findOne({ title: { $regex: new RegExp(categoryObj.title, "i") } });
    if (category) {
      res.status(401)
        .json({
          error: 'Category already exists!',
        })
    } else {
      Category.create(categoryObj)
        .then((category) => {
          res.status(200)
            .json({
              message: 'Category created successfully!',
              category
            })
        })
        .catch((error) => {
          if (!error.statusCode) {
            error.statusCode = 500;
          }
          next(error);
        });
    }
  }
}