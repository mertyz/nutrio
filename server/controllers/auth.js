const { validationResult } = require('express-validator/check');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const encryption = require('../util/encryption');

function validateUser(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({
      message: 'Validation failed, entered data is incorrect',
      errors: errors.array()
    });
    return false;
  }

  return true;
}

module.exports = {
  signUp: (req, res, next) => {

    if (validateUser(req, res)) {
      const { username, email, firstName, lastName, password } = req.body;
      const salt = encryption.generateSalt();
      const hashedPassword = encryption.generateHashedPassword(salt, password);
      User.create({
        email,
        hashedPassword,
        username,
        firstName,
        lastName,
        salt
      }).then((user) => {
        res.status(201)
          .json({ message: 'User created!', userId: user._id, username: user.username });
      })
        .catch((error) => {
          if (!error.statusCode) {
            error.statusCode = 500;
          }

          next(error);
        });
    }
  },
  signIn: async (req, res, next) => {
    const { username, password } = req.body;

    User.findOne({ username })
      .then((user) => {
        if (!user) {
          const error = new Error('A user with this username could not be found');
          error.statusCode = 401;
          throw error;
        }

        if (!user.authenticate(password)) {
          const error = new Error('Wrong password');
          error.statusCode = 401;
          throw error;
        }

        const token = jwt.sign({
          username: user.username,
          userId: user._id.toString()
        }
          , 'somesupersecret'
          , { expiresIn: '1h' });
        res.status(200).json(
          {
            message: 'User successfully logged in!',
            token,
            userId: user._id.toString(),
            username: user.username,
            isAdmin: user.roles.indexOf('Admin') !== -1
          });
      })
      .catch(error => {
        if (!error.statusCode) {
          error.statusCode = 500;
        }
        error.message = 'There was an error with your Email/Password combination. Please try again.';

        next(error);
      });
  },
  order: (req, res, next) => {
    let username = req.body.username;
    let orders = req.body.cart;
    User.findOne({ username }).then((user) => {
      let total = 0;
      let orderObj = {};
      orderObj.products = [];
      for (const order of orders) {
        let title = order.title;
        let quantity = order.quantity;
        let price = order.price;
        total += (order.price * quantity);
        orderObj.products.push({
          title,
          quantity,
          price
        })
      }
      orderObj.VAT = total * 0.2;
      total = total + (total * 0.2);
      if (total > 100) {
        orderObj.delivery = 0;
      } else {
        total += 5;
        orderObj.delivery = 5;
      }
      orderObj.total = total;
      user.orders.push(orderObj);
      user.save();
      res
        .status(200)
        .json({ message: 'Order completed succesfully.', user });
    }).catch(error => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.message = 'User was not found.';

      next(error);
    });
  },
  getOrders: (req, res, next) => {
    let username = req.params.username;
    User.findOne({ username }).then(user => {
      let orders = user.orders;
      res
        .status(200)
        .json({ message: "Fetched orders succesfully", orders })
    }).catch(error => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.message = 'User was not found.';
      next(error);
    })
  }
};