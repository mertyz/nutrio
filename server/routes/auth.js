const router = require('express').Router();
const { body } = require('express-validator/check');
const authController = require('../controllers/auth');
const User = require('../models/User');

router.post('/signup',
  [
    // TODO: Add normalize email and check
    body('username')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Please enter a valid username.')
      .custom((value, { req }) => {
        return User.findOne({ username: value }).then(userDoc => {
          if (userDoc) {
            return Promise.reject('User already exists!');
          }
        })
      }),
    body('email')
      .isEmail()
      .withMessage('Please enter a valid email.')
      .custom((value, { req }) => {
        return User.findOne({ email: value }).then(userDoc => {
          if (userDoc) {
            return Promise.reject('E-Mail address already exists!');
          }
        })
      }),
    body('firstName')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Please enter first name.'),
    body('lastName')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Please enter last name.'),
    body('password')
      .trim()
      .isLength({ min: 5 })
      .withMessage('Please enter a valid password.')
      .custom((value, { req, loc, path }) => {
        if (value !== req.body.repeatPassword) {
          return false;
        } else {
          return value;
        }
      }).withMessage("Passwords don't match."),
  ]
  , authController.signUp);
router.post('/signin', authController.signIn);
router.post('/order', authController.order);
router.get('/user/orders/:username', authController.getOrders);

module.exports = router;
