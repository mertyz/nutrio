const router = require('express').Router();
const feedController = require('../controllers/feed');
const isAuth = require('../middleware/is-auth');

router.get('/products', feedController.getProducts);
router.post('/product/create', feedController.createProduct);
router.get('/product/:id', feedController.getProductById);
router.post('/edit/:id', feedController.postEditProduct);
router.post('/remove/:id', feedController.postDeleteProduct);
router.get('/categories', feedController.getCategories);
router.post('/category/create', feedController.createCategory);
router.get('/category/:id', feedController.getCategoryById);
router.get('/category/filter/:link', feedController.getCategoryProducts);


module.exports = router;